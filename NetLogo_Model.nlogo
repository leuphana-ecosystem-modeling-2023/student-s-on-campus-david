;Model of student´s willingness to attend university and their movement patterns on the Leuphana Campus
;@authors David Mbang <david.mbang@stud.leuphana.de>
;@copyright 2023 David Mbang
;@date 2023-08-20

globals [
  white-patch          ; Important to set pcolor of concerned patches white to setup the campus map
  turtle-count         ; Counts the number of turtles
  hours                ; The simulated hours in the model
  minutes              ; The simulated minutes in the model
]

patches-own [
  category             ; Different areas on campus
  walkability          ; Whether turtles can walk on concerned patches
]

turtles-own [
  tick-count
  energy
]


;======================================================================================================
; Setup of the campus
;======================================================================================================




to set-white-patch                    ; Needed in order to be able to differentiate the different elements of the campus from the white background
    set white-patch patches with [    ; All patches with a pclolor representing white are selected
    pcolor >= 8 and pcolor < 10 or
    pcolor >= 18 and pcolor < 20 or
    pcolor >= 28 and pcolor < 30 or
    pcolor >= 38 and pcolor < 40 or
    pcolor >= 48 and pcolor < 50 or
    pcolor >= 58 and pcolor < 60 or
    pcolor >= 68 and pcolor < 70 or
    pcolor >= 78 and pcolor < 80 or
    pcolor >= 88 and pcolor < 90 or
    pcolor >= 98 and pcolor < 100 or
    pcolor >= 108 and pcolor < 110 or
    pcolor >= 118 and pcolor < 120 or
    pcolor >= 128 and pcolor < 130 or
    pcolor >= 138 and pcolor < 140
  ]
  ask white-patch [                     ; Different pcolors are changed to white
    set pcolor white
  ]
end



to assign-categories                     ; Names differen elements of the campus (e.g. Mensa, Mensawiese, Klippo; Mensa, etc)

 set-current-directory "./"              ; Directory for images is set. Adapt this to the folder that you saved the "Kollage" images to
  let categories (list "bib" "mensa" "mensawiese" "work_outside" "nature" "karacho" "cafe" "mensa_tables" "no_interaction" "penny" "rest" "walkway" "exercise" "study")
  ; All categories except "paths" because we will assign it in a different way
  foreach categories [i ->
    import-pcolors (word i ".png")
    set-white-patch                      ; The whole map is turned white
    ask patches [
      if pcolor != white [               ; pcolor of the image, which is not white, will be assigned the respective category
        set category i
      ]
    ]
  ]
  ask patches [
    if category = 0 [                    ; All remaining patches become paths
      set category "path"
    ]
  ]
end




to assign-colors                        ; gives the categories distinctive colors
  ask patches [                         ; depending on the previously assigned category we will change the pcolor of the patches of the different elements of the campus
    if category = "bib"[                ; pcolor has been chosen subjectively and doesn't represent the real coloring
      set pcolor 12
    ]
    if category = "mensa" [
      set pcolor 51
    ]
    if category = "mensawiese" [
      set pcolor 65
    ]
    if category = "work_outside" [
      set pcolor 95
    ]
    if category = "nature" [
      set pcolor 65
    ]
    if category = "karacho" [
      set pcolor 45
    ]
    if category = "cafe" [
      set pcolor 45
    ]
    if category = "mensa_tables" [
      set pcolor 95
    ]
    if category = "no_interaction" [
    set pcolor 5
    ]
    if category = "penny" [
      set pcolor 15
    ]
    if category = "rest" [
      set pcolor 95
    ]
    if category = "study" [
      set pcolor 0
    ]
    if category = "walkway" [
      set pcolor 35
    ]
    if category = "exercise" [
      set pcolor 26
    ]
  ]
end



;======================================================================================================
; Startup procedure
;======================================================================================================

To startup
clear-all
  ask patches [
    set pcolor white                   ; Images that will be loaded into NetLogo have a white background "clear-all" turns surface into black background but patches need to be white to make working with them easier in the end
  ]
  assign-categories
  assign-colors
  reset-ticks
end

;======================================================================================================
; Setup procedures
;======================================================================================================

to setup
  ask turtles [
    die
  ]
  setup-people                        ; creates students
  setup-walkablity                    ; Defines patches students can walk on
  setup-weather                       ; Arbitrarily fixes initial temperature
  set turtle-count count turtles
  setup-students-plot                 ; Creates plot to count students
  setup-time
  reset-ticks
end


to setup-people                       ; creates 500 turtles (students) that are randomly placed on walkway & path
  create-turtles 500 [                ; number of turtles can be adapted to specific needs
  set shape "person"]                 ; give turtles a person shape
  ask turtles [
    move-to one-of patches with [category = "path" or category = "walkway" or category = "mensawiese" or category = "nature" or category = "rest"]
    set energy (random 500) + 500     ; Turtles are given a random energy level between 500 and 1000

  ]
end

to setup-walkablity                    ; Defines the patches that turtles are allowed to walk on
  ask patches [
    if category = "path" or
    category = "walkway" or
    category = "mensawiese" or
    category = "nature" or
    category = "rest" [
      set walkability true             ; Turtles are able to walk on these patches
    ]
  ]
end


to setup-weather
  set temperature 12                   ; This value in °C can be adapted as needed
end

to setup-students-plot
  clear-plot
  set-plot-y-range 0 turtle-count      ; Count turtles on the y-axis
  set-current-plot "Number of Students on Campus"
  set-current-plot-pen "Students"
  plot count turtles
end


to setup-time
  set hours 8                           ; Model starts at 8:00am
  set minutes 0
end

;======================================================================================================
; Go commands
;======================================================================================================

to go
  advance-time                          ; Clock-time ticks
  plot-students
  if hours = 22 [                       ; The model stops at 22:00 in the evening
    stop
  ]
  ask turtles [
   walk
   check-leave-return-world
  ]
  tick
end


to advance-time
 if ticks mod 100 = 0 [                 ;every 100 ticks one minutes passes
    set minutes minutes + 1
  ]
  if minutes mod 60 = 0 [               ;every 60 minutes one hours passes
    set hours hours + 1
    set minutes 0
  ]
end


to plot-students                        ; Plots the number of students
  plot count turtles
end


to lose-random-energy
  let energy-loss random 3 + 1          ; Generate a random number between 1 and 3
    set energy energy - energy-loss     ; Decrease energy level by the random amount
end


to check-leave-return-world
    let leave-probability random-float 1.0       ; Generate a random probability between 0.0 and 1.0
    if leave-probability < 0.01 [                ; Adjust the threshold as needed
      leave-world
    ]

    let return-probability random-float 1.0      ; Generate a random probability between 0.0 and 1.0
    if return-probability < 0.01 [               ; Adjust the threshold as needed
      return-to-world
    ]

end

to leave-world                                   ; Students leave campus
  move-to one-of patches with [
    pxcor = min-pxcor or
    pxcor = max-pxcor or
    pycor = min-pycor or
    pycor = max-pycor
  ]
  die
end

to return-to-world                               ; Students get back on campus
  hatch 1
  move-to one-of patches with [
    category = "path" or
    category = "walkway"
  ]
end

;==================================================================================================================================================
; walk command
;==================================================================================================================================================


to walk
  if temperature >= -10 and temperature <= 0 [        ; Students behave in a specific way when temperature lies between -10 and 0 °C
    if count turtles > 100 [                          ; Leave-world is only activated when there are more than 100 students on campus
    let leave-probability random-float 1.0
    if leave-probability < 0.02 [                     ; When it´s so cold the probability to leave campus is twice as high as the probability to return to campus (These values can be adapted as needed)
      leave-world
    ]
    ]
    let return-probability random-float 1.0
    if return-probability < 0.01 [
      return-to-world
    ]
  ifelse [walkability] of patch-ahead 1 = true [
    move-to patch-ahead 1                              ; Students walk on patches that are walkable
    lose-random-energy                                 ; After every step students lose random energy
    rt random 3                                        ; After each step students rotate to the right in a random angle between 0 and 3 degrees to avoid them walking in straight lines forever
  ] [
    rt random 90                                       ; If patch-ahead 1 is not walkable students rotate to the right in a random angle between 0 and 90 degrees until they face a patch that is walkable
  ]
  ]

  if temperature >= 1 and temperature <= 10  [         ; Students behave in a specific way when temperature lies between 0 and 10°C
    if count turtles > 200 [                           ; Leave-world is only activated when there are more than 200 students on campus
    let leave-probability random-float 1.0
    if leave-probability < 0.017 [                     ; At these temperatures the leave probability is more than 1,5 times higher than the return probability
      leave-world
    ]
    ]
    let return-probability random-float 1.0
    if return-probability < 0.01 [
      return-to-world
    ]
  ifelse [walkability] of patch-ahead 1 = true [
    move-to patch-ahead 1
    lose-random-energy
    rt random 3
  ] [
    rt random 90
  ]
  ]

  if temperature >= 11 and temperature <= 20  [         ; Students behave in a specific way when temperature lies between 11 and 20°C
    let leave-probability random-float 1.0
    if leave-probability < 0.01 [                        ; At these temperatures the probability to return to campus is a little higher than the probability to leave campus
      leave-world
    ]
    if count turtles < 1200 [                            ; When the number of students reaches 1200 no new students arrive on campus (This value can be adjusted as needed)
    let return-probability random-float 1.0
    if return-probability < 0.015 [                      ; Because the return-probability is higher than the leave-probability more turtles should appear on campus than disappear
      return-to-world
    ]
    ]
    ifelse [walkability] of patch-ahead 1 = true [
    move-to patch-ahead 1
    lose-random-energy
    rt random 3
  ] [
    rt random 90

  ]
  ]

  if temperature >= 21 and temperature <= 30  [           ; Students behave in a specific way when temperature lies between 21 and 30°C
    let leave-probability random-float 1.0                ; At these temperatures the probability to return to campus is 1,5 times higher than the probability to leave campus
    if leave-probability < 0.01 [
      leave-world
    ]
    if count turtles < 1500 [                             ; When the number of students reaches 1500 no new students arrive on campus
    let return-probability random-float 1.0
    if return-probability < 0.012[
      return-to-world
    ]
    ]
    ifelse [walkability] of patch-ahead 1 = true [
    move-to patch-ahead 1
    lose-random-energy
    rt random 3
  ] [
    rt random 90

  ]
  ]
   if temperature >= 31 and temperature <= 40  [          ; Students behave in a specific way when temperature lies between 31 and 40°C
    if count turtles > 500   [                            ; Leave-world is only activated when there are more than 500 students on campus
    let leave-probability random-float 1.0                ; At these temperatures the probability to leave campus is 1,2 times higher than the probability to return to campus
    if leave-probability < 0.012 [
      leave-world
    ]
    ]

    let return-probability random-float 1.0
    if return-probability < 0.01[
      return-to-world
    ]
    ifelse [walkability] of patch-ahead 1 = true [
    move-to patch-ahead 1
    lose-random-energy
    rt random 3
  ] [
    rt random 90

  ]
  ]

 if energy  >= 600 and energy <= 1000 [
    let exercise-probability random-float 1.0
    if exercise-probability < 0.2  [                       ; 20 % of students with energy level between 600 and 1000 exeute the exercise command
    exercise
  ]
  ]

  if energy > 300 [
    let study-probability random-float 1.0
    if study-probability < 0.5  [                          ; 50% of students with an energy level above 300 ecevute the study command
    study
    ]
  ]

   if energy < 500 [
    let snack-probability random-float 1.0
    if snack-probability < 0.2  [                          ; 20% of students execute the snack command
    snack
    ]
  ]

   if energy < 300 [
    let eat-probability random-float 1.0
    if eat-probability < 0.3  [                            ; 30% of students with energy level < 300 execute the eat command
    eat
    ]
  ]

  if energy < 100 [
    let return-probability random-float 1.0
    if return-probability < 0.001  [                       ; 0,1% of students with energy level below 100 execute the leave-world command
    leave-world
    ]
  ]




end

;======================================================================================================
; Eat command
;======================================================================================================

to eat
  let end-patch one-of patches with [
    category = "mensa"                                      ; The destination of the students who follow the "eat" command is the "mensa"
  ]
  if [category] of patch-here != "mensa" [
    ifelse [walkability] of patch-ahead 1 = true [           ; If the patch-ahead 1 is walkable the turtle moves on to this patch
      move-to patch-ahead 1
      face end-patch

   ][
      rt random 90                                           ; If the patch-ahead is not walkable the turtles turn right in a random angle between 0 and 90°
    ]
    if [category] of patch-ahead 1 = "mensa" [
      move-to patch-ahead 1
      set energy energy + 500                                 ; recharged energy can be adjusted as needed

  ]

  ]


end

;======================================================================================================
; Exercise command
;======================================================================================================

to exercise
  let end-patch one-of patches with [
    category = "mensawiese" or
    category = "exercise"                                      ; The destination of turtles executing the "exercise" command is either "mensawiese" or "exercise"
  ]
  if [category] of patch-here != "mensawiese" or [category] of patch-here != "exercise" [
    ifelse [walkability] of patch-ahead 1 = true [
      move-to patch-ahead 1
      face end-patch
  ][
      rt random 90
    ]
    if [category] of patch-ahead 1 = "mensawiese" or [category] of patch-ahead 1 = "exercise" [
      move-to patch-ahead 1
      set energy energy - random 100                            ; expended energy can be adjusted as needed
    ]
  ]
end

;======================================================================================================
; Snack command
;======================================================================================================

to snack

 let end-patch one-of patches with [
    category = "karacho" or
    category = "cafe" or
    category = "penny"                                          ; The destination of students who execute the "snack" command is "karacho", "cafe" or "penny"
  ]
  if [category] of patch-here != "karacho" or [category] of patch-here != "cafe" or [category] of patch-here != "penny" [
    ifelse [walkability] of patch-ahead 1 = true [
      move-to patch-ahead 1
      face end-patch
  ][
      rt random 90
    ]
    if [category] of patch-ahead 1 = "karacho" or [category] of patch-ahead 1 = "cafe" or [category] of patch-ahead 1 = "penny" [
      move-to patch-ahead 1
      set energy energy + (random 200) + 50                      ; Recharged energy can be adjusted as needed
    ]
  ]

end

;======================================================================================================
; Study  command
;======================================================================================================

to study
   let end-patch one-of patches with [
    category = "bib" or
    category = "work_outside" or
    category = "study"                                           ; The destination of students who execute the "study" command is "bib",  "work_outside" or "study"
  ]
  if [category] of patch-here != "bib" or [category] of patch-here != "work_outside" or [category] of patch-here != "study" [
    ifelse [walkability] of patch-ahead 1 = true [
      move-to patch-ahead 1
      face end-patch
  ][
      rt random 90
    ]
    if [category] of patch-ahead 1 = "bib" or [category] of patch-ahead 1 = "work_outside" or [category] of patch-ahead 1 = "study" [
      move-to patch-ahead 1
      set energy energy - random 100                              ; Expended energy can be adjusted as needed
    ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
385
20
1017
445
-1
-1
2.0731
1
10
1
1
1
0
1
1
1
-150
150
-100
100
1
1
1
ticks
30.0

BUTTON
15
25
82
58
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
115
25
185
58
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
15
180
350
440
Number of Students on Campus
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"Students" 1.0 0 -16777216 true "" "plot count turtles"

MONITOR
15
70
72
115
O´Clock
hours
17
1
11

MONITOR
75
70
132
115
Minutes
minutes
17
1
11

SLIDER
15
130
187
163
temperature
temperature
-10
40
12.0
1
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

This model demonstrates the number of students that attend the Leuphana university campus in relation to outside temperature. Student´s movements on campus are influenced by their energy.

## HOW IT WORKS
To upload the campus map please save the attached images in a folder and set the working directory to that folder. Be careful to maintain the names of the images as they are in the code.


There are two main variations in this model.

In the first variation, the outside temperature influences the number of students on Campus. At certain temperature levels more students appear on campus than there are students who leave the campus. At other temperatures there are more students who leave the campus than there are students who appear on campus.

In the second variation, the student´s current energy influences where they move on the campus. There are different energy thresholds in which students perform activities.

## HOW TO USE IT

Click the SETUP button to set up the students on campus (dispersed on the walkable patches all over the campus). Click the GO button to start the simulation. The number of students is shown in the PLOT.

The TEMPERATURE slider controls the outside temperature on campus. Use the slider to see how changing temperature influence the number of students on campus.

## THINGS TO NOTICE

Observe how the number of students evolves with different temperatures. Where do the students seem to gather most? 
Does the number of students on campus at a certain temperature stay constant or are there variations? 


## THINGS TO TRY

Go to the code and play around with the energy tresholds that determine the student´s movement patterns on campus. Do they move to different places more? 
You can also change the probabilities of students arriving on and leaving campus at different temperatures. 

## EXTENDING THE MODEL

Make student´s movements on campus dependent on temperature too. 
Differentiate between mental energy and physical energy to assess the qualitative differences between physical and mental activity. 
Create opening hours and staying times for the different buildings to represent reality even closer. 
Give the students a course schedule and use the clock monitors to track student´s behaviour over a period of time. 

## NETLOGO FEATURES

The graphical implementation of the Leuphana map attributes each patch a color and category and hence allows the turtles to interact with the patches. Through this the different categories of patches could also be attributed different properties that influence the students. For example every mensa patch could automatically be associated with a random 300 increase in energy for each turtle that gets in contact with it.

## RELATED MODELS

The "slime mold campus navigation" model is a model that also represents the Leuphana campus. Here the shortest ways from A to B for students can be explored. 

## CREDITS AND REFERENCES

Copyright 2023 David Mbang
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.3.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
